﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Practice_29_11
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Process[] processes;
        List<ProcessView> processViews;
        public MainWindow()
        {
            InitializeComponent();
            Refresh();
        }

        private void Refresh()
        {       
            processes = Process.GetProcesses();

            var processViews = new List<ProcessView>();
            foreach(var process in processes)
            {
                processViews.Add(new ProcessView(process));
            }
            processesGrid.ItemsSource = processViews;
        }
        

        private void KillButtonClick(object sender, RoutedEventArgs e)
        {
            if (processesGrid.SelectedItem == null)
            {
                MessageBox.Show("Выберите процесс!");
            }
            else
            {
                try
                {
                    int id = ((ProcessView)processesGrid.SelectedItem).ProcessId;
                    Process toKill = processes.Where(prc => prc.Id == id).FirstOrDefault();
                    toKill.Kill();       
                }
                /*catch(InvalidCastException exception)
                {
                    MessageBox.Show($"Произошла непредвиденная ошибка!\n{exception.Message}")
                }*/
                catch(ArgumentNullException exception)
                {
                    MessageBox.Show($"Произошла непредвиденная ошибка!\n{exception.Message}")
                }
                catch (System.ComponentModel.Win32Exception exception)
                {
                    MessageBox.Show($"Невозможно удалить процесс!\n{exception.Message}");
                }
                catch (NotSupportedException exception)
                {
                    MessageBox.Show($"Невозможно удалить процесс!\n{exception.Message}");
                }
                catch (InvalidOperationException exception)
                {
                    MessageBox.Show($"Невозможно удалить процесс!\n{exception.Message}");
                }
            }
            Refresh();
        }

        private void refreshButtonClick(object sender, RoutedEventArgs e)
        {
            Refresh();
        }
    }
}
