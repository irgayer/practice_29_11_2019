﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Practice_29_11
{
    public class ProcessView
    {
        public String ProcessName { get; set; }
        public int ProcessId { get; set; }
        public String Responding { get; set; }
        public int SessionId { get; set; }

        public ProcessView(Process process)
        {
            ProcessId = process.Id;
            ProcessName = process.ProcessName;
            Responding = process.Responding.ToString();
            SessionId = process.SessionId;
        }
    }
}
